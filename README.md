# Blender 3D Presentation Plug-in

Welcome to the Blender 3D Presentation Plug-in. It can be used to create and present a 3D presentation in Blender.


## Installation

- Download the [presentation_plugin.py](https://gitlab.fit.cvut.cz/sojkamat/blender-presentation-plug-in/blob/master/presentation_plugin.py)
- In Blender go to Edit - Preferences - Add-ons - Install...
- Select the presentation_plugin.py file
- Check the box to enable the add-on
- Press N in the 3D Viewport for the plug-in UI  (Components, Presentation, Slideshow) 

## Workspaces

- Download the [PRESENTATION_TEMPLATE.blend](https://gitlab.fit.cvut.cz/sojkamat/blender-presentation-plug-in/blob/master/PRESENTATION_TEMPLATE.blend)
- Open it and switch to the "Layout" workspace
- Go to File - Defualts - Save Startup File
- Now you can use the workspaces Components, Presentation, Slideshow and Presenter from any .blend file

## How to use

- You can create a presentation from scratch in a single .blend file, or put together more "Components" to form a presentation
- Components are separate .blend files, typically animated
- One Component.blend file always has one camera object
- Use timeline markers to define stop points or loops
- Presentation is a separate .blend file
- Use "Choose Components" in the Presentation UI Tab to import Components
- Use "Override" to enable changes in the presentation
- After any changes, use "Recalculate Cameras" 
- Use F5 to start the presentation, F6 to end it
- Use PageUp, PageDown for presenting (Next, Previous)
- Check the [plugin-maunal](https://gitlab.fit.cvut.cz/sojkamat/blender-presentation-plug-in/blob/master/plugin_manual.adoc) for more info

## Showcase and Tutorial

- Click the image below to see the full playlist.

<a href="https://www.youtube.com/playlist?list=PLE6-q8zVbelB2T_KwjYNCRl2FmUT8CBsJ
" target="_blank"><img src="https://raw.githubusercontent.com/MatyasSojka/Blender-Presentation-Plug-in/main/Screenshot_5.png" 
alt="Plug-in Showcase and Tutorial" width="480" height="270" border="10" /></a>

